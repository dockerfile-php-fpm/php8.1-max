#!/bin/bash
if [[ $1 = "stop" ]]
then 
        kill `ps -ef | grep -e "tail -f /dev/null" | awk '{print $2}'`
        exit
fi 
if [[ $1 = "restart" ]]
then
        service php8.1-fpm restart
else
        service php8.1-fpm start
        tail -f /dev/null 
fi