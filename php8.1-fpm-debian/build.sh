#!/bin/bash
TAG=$(date '+%g%m%d%H%M')

docker build -t ablaikhans/php-fpm:8.1-debian-$TAG .
docker push ablaikhans/php-fpm:8.1-debian-$TAG